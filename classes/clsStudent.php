<?php
    class clsStudent {

        public function __construct() {
			//Call the databaseconnection
			$this->connection = database::connect();
		}


        public function getResults() {
            $query = "SELECT *  FROM tb_results r, tb_student s, tb_criteria c 
                                WHERE r.student_uuid = s.student_uuid
                                AND r.criteria_id = c.id";

            $data = $this->connection->query($query);

            $output = '<table width="70%" border="1" cellpadding="5" cellspacing="5"
                    <tr>
                        <th>Resultaten</th>
                        <th>Student Naam</th>
                        <th>Criteria</th>
                        <th>Geslaagd</th>
                        <th>Status</th>
                    </tr>';

            foreach($data as $row) {
                $name = $row['firstname'] . " " . $row['middlename'] . " " . $row['lastname'];

                $output .= '<tr>
                        <td>' .$row["result_id"] . '</td>
                        <td>' .$name. '</td>
                        <td>' .$row["criteria"] . '</td>
                        <td>' .$row["checked"] . '</td>
                        <td>' .$row["status"] . '</td>
                    </tr>';          
            }
            $output .= '</table>';
            return $output;
        }
    }
?>