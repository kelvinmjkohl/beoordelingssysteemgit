<?php
	class page {

		public function __construct() {
			//Call the databaseconnection
			$this->connection = database::connect();
		}

		public function getHtml() {
			$output = "Dit zijn de resultaten:<br /><br />";
			$output .= $this->getResults();
			return $output;
		}

		private function getResults() {
			require_once(CLASSES_PATH . "clsStudent.php");
			$objStudent = new clsStudent();
			return $objStudent->getResults();
		}
	}