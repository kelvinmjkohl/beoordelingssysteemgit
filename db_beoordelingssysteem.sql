-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jun 11, 2019 at 09:28 AM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_beoordelingssysteem`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_criteria`
--

CREATE TABLE `tb_criteria` (
  `id` int(11) NOT NULL,
  `form_uuid` varchar(40) NOT NULL COMMENT 'Zie tabel tb_form',
  `criteria` text NOT NULL COMMENT 'Het echte criteria waarop beoordeeld wordt',
  `level_id` int(11) NOT NULL COMMENT 'Basis, voldoende, goed, excellent',
  `value` int(11) NOT NULL COMMENT 'Aantal punten voor dit criteria, dus waardebepaling',
  `indexnumber` int(11) NOT NULL COMMENT 'Volgnummer voor verschijnen op formulier, 1 staat bovenaan, hoogste nummer bovenaan',
  `status` int(11) NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Beoordelingscriteria per formulier, zie tb_formulier';

--
-- Dumping data for table `tb_criteria`
--

INSERT INTO `tb_criteria` (`id`, `form_uuid`, `criteria`, `level_id`, `value`, `indexnumber`, `status`, `timestamp`) VALUES
(1, 'd6011a8f-4b13-4eb4-8380-0fd86f2bb510', 'Student moet de basisstructuur <html><head><body> op de juiste wijze hebben toegepast.', 1, 1, 1, 1, '2019-06-07 12:22:10'),
(2, 'd6011a8f-4b13-4eb4-8380-0fd86f2bb510', 'Student moet overal de juiste </...> <... /> sluittags hebben toegepast, zoals HTML5 dat voorschrijft.', 1, 1, 2, 1, '2019-06-07 12:22:10');

-- --------------------------------------------------------

--
-- Table structure for table `tb_educationname`
--

CREATE TABLE `tb_educationname` (
  `id` int(11) NOT NULL,
  `educationname` varchar(200) NOT NULL COMMENT 'Alle opleidingen die Vista aanbiedt',
  `level` int(11) NOT NULL COMMENT 'Opleidingsniveau',
  `status` int(11) NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabel met opleidingen, vakken aan opelding koppelen';

--
-- Dumping data for table `tb_educationname`
--

INSERT INTO `tb_educationname` (`id`, `educationname`, `level`, `status`, `timestamp`) VALUES
(1, 'Applicatie- en mediaontwikkelaar', 4, 1, '2019-06-07 12:04:50'),
(2, 'ICT beheerder', 4, 1, '2019-06-07 12:04:50'),
(3, 'Medewerker beheer ICT', 3, 1, '2019-06-07 12:06:22'),
(4, 'Medewerker ICT', 2, 1, '2019-06-07 12:06:22'),
(5, 'Aankomend medewerker grondoptreden', 2, 1, '2019-06-07 12:07:06'),
(6, 'Allround kapper', 3, 1, '2019-06-07 12:07:06');

-- --------------------------------------------------------

--
-- Table structure for table `tb_form`
--

CREATE TABLE `tb_form` (
  `form_uuid` varchar(40) NOT NULL,
  `formname` varchar(200) NOT NULL COMMENT 'Willekeurige naam voor dit formulier',
  `schoolsubject` varchar(200) NOT NULL COMMENT 'Naam van het vak',
  `topic` varchar(200) NOT NULL COMMENT 'Onderwerp binnen het vak',
  `status` int(11) NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabel met voorgedefinieerde formulieren mbt vak en onderwerp';

--
-- Dumping data for table `tb_form`
--

INSERT INTO `tb_form` (`form_uuid`, `formname`, `schoolsubject`, `topic`, `status`, `timestamp`) VALUES
('3b22f1be-ed82-4561-9994-eb1452676450', 'PHP periode 2', 'PHP', 'Basisvaardigheden', 1, '2019-06-07 12:19:59'),
('d6011a8f-4b13-4eb4-8380-0fd86f2bb510', 'HTML periode 1', 'HTML', 'Basisvaardigheden', 1, '2019-06-07 12:19:59');

-- --------------------------------------------------------

--
-- Table structure for table `tb_grouprights`
--

CREATE TABLE `tb_grouprights` (
  `grouprights_id` int(11) NOT NULL,
  `role_uuid` varchar(40) NOT NULL,
  `groupname` varchar(30) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_koppel_group_rights`
--

CREATE TABLE `tb_koppel_group_rights` (
  `id` int(11) NOT NULL,
  `grouprights_id` int(11) NOT NULL,
  `rights_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_level`
--

CREATE TABLE `tb_level` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL COMMENT 'Basis, voldoende, goed, excellent',
  `status` int(11) NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_level`
--

INSERT INTO `tb_level` (`id`, `name`, `status`, `timestamp`) VALUES
(1, 'basis', 1, '2019-06-07 11:57:38'),
(2, 'voldoende', 1, '2019-06-07 11:57:38'),
(3, 'goed', 1, '2019-06-07 11:57:50'),
(4, 'excellent', 1, '2019-06-07 11:57:50');

-- --------------------------------------------------------

--
-- Table structure for table `tb_results`
--

CREATE TABLE `tb_results` (
  `result_id` int(11) NOT NULL,
  `student_uuid` int(11) NOT NULL COMMENT 'Komt uit tabel tb_student',
  `criteria_id` int(11) NOT NULL,
  `checked` int(11) NOT NULL DEFAULT '0' COMMENT 'Is dit criteria behaald. 1 is True, 0 is False',
  `status` int(11) NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabel met beoordelingsresultaten';

-- --------------------------------------------------------

--
-- Table structure for table `tb_rights`
--

CREATE TABLE `tb_rights` (
  `rights_id` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_role`
--

CREATE TABLE `tb_role` (
  `rol_uuid` varchar(40) NOT NULL,
  `rolename` varchar(30) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabel met de rollen die voorkomen in de applicatie';

-- --------------------------------------------------------

--
-- Table structure for table `tb_schoolsubject`
--

CREATE TABLE `tb_schoolsubject` (
  `id` int(11) NOT NULL,
  `schoolsubject` varchar(200) NOT NULL COMMENT 'Vaknaam',
  `educationname_id` int(11) NOT NULL COMMENT 'Id van opleiding, komt uit tb_educationname',
  `status` int(11) NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Selecteer hieruit je vakken';

--
-- Dumping data for table `tb_schoolsubject`
--

INSERT INTO `tb_schoolsubject` (`id`, `schoolsubject`, `educationname_id`, `status`, `timestamp`) VALUES
(1, 'PHP', 1, 1, '2019-06-07 12:10:17'),
(2, 'HTML', 1, 1, '2019-06-07 12:10:17'),
(3, 'JavaScript', 1, 1, '2019-06-07 12:10:30'),
(4, 'CSS', 1, 1, '2019-06-07 12:10:30'),
(5, 'C#', 1, 1, '2019-06-07 12:10:43'),
(6, 'Databases', 1, 1, '2019-06-07 12:10:43'),
(7, 'OOP met PHP', 1, 1, '2019-06-07 12:11:01'),
(8, 'OOP met C#', 1, 1, '2019-06-07 12:11:01');

-- --------------------------------------------------------

--
-- Table structure for table `tb_student`
--

CREATE TABLE `tb_student` (
  `student_uuid` varchar(40) NOT NULL,
  `user_uuid` varchar(40) NOT NULL COMMENT 'Komt uit tabel tb_users',
  `studentnumber` int(11) NOT NULL,
  `firstname` varchar(80) NOT NULL,
  `middlename` varchar(20) NOT NULL,
  `lastname` varchar(120) NOT NULL,
  `educationname_id` int(11) NOT NULL,
  `cohort` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabel met studenten';

-- --------------------------------------------------------

--
-- Table structure for table `tb_users`
--

CREATE TABLE `tb_users` (
  `user_uuid` varchar(40) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(100) NOT NULL,
  `screenname` varchar(20) NOT NULL,
  `start_date` date NOT NULL COMMENT 'datum waarop gebruik actief moet worden',
  `end_date` date NOT NULL COMMENT 'datum waarop gebruiker geen toegang meer heeft tot applicatie',
  `verify_hash` varchar(120) NOT NULL COMMENT 'Wordt gebruikt bij bijvoorbeeld e-mail verify',
  `verify_end_date` date NOT NULL COMMENT 'geeft einddatum verify check aan, zodat een eventuele link verloopt',
  `status` int(11) NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabel met gebruikers van het systeem';

--
-- Dumping data for table `tb_users`
--

INSERT INTO `tb_users` (`user_uuid`, `email`, `password`, `screenname`, `start_date`, `end_date`, `verify_hash`, `verify_end_date`, `status`, `timestamp`) VALUES
('420df743-d9b2-49d8-beed-e6431b38354f', 'erik', '$2y$12$ahxWOuYIxLdqQDoQ5ptYuel76a.bm/jgUtsrlhroaqWto/2u2tIwS', 'Erik Steens', '2019-06-02', '2099-06-02', '$2y$12$ahxWOuYIxLdqQDoQ5ptYuel76a.bm/jgUtsrlhroaqWto/2u2tIwS', '2019-07-02', 1, '2019-06-09 23:53:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_criteria`
--
ALTER TABLE `tb_criteria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `form_uuid` (`form_uuid`),
  ADD KEY `level_id` (`level_id`);

--
-- Indexes for table `tb_educationname`
--
ALTER TABLE `tb_educationname`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_form`
--
ALTER TABLE `tb_form`
  ADD PRIMARY KEY (`form_uuid`);

--
-- Indexes for table `tb_grouprights`
--
ALTER TABLE `tb_grouprights`
  ADD KEY `role_uuid` (`role_uuid`);

--
-- Indexes for table `tb_koppel_group_rights`
--
ALTER TABLE `tb_koppel_group_rights`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grouprights_id` (`grouprights_id`),
  ADD KEY `rights_id` (`rights_id`);

--
-- Indexes for table `tb_level`
--
ALTER TABLE `tb_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_results`
--
ALTER TABLE `tb_results`
  ADD PRIMARY KEY (`result_id`),
  ADD KEY `criteria_id` (`criteria_id`);

--
-- Indexes for table `tb_rights`
--
ALTER TABLE `tb_rights`
  ADD PRIMARY KEY (`rights_id`);

--
-- Indexes for table `tb_role`
--
ALTER TABLE `tb_role`
  ADD PRIMARY KEY (`rol_uuid`);

--
-- Indexes for table `tb_schoolsubject`
--
ALTER TABLE `tb_schoolsubject`
  ADD PRIMARY KEY (`id`),
  ADD KEY `educationname_id` (`educationname_id`);

--
-- Indexes for table `tb_student`
--
ALTER TABLE `tb_student`
  ADD PRIMARY KEY (`student_uuid`),
  ADD KEY `studentnumber` (`studentnumber`),
  ADD KEY `lastname` (`lastname`),
  ADD KEY `User_uuid` (`user_uuid`);

--
-- Indexes for table `tb_users`
--
ALTER TABLE `tb_users`
  ADD PRIMARY KEY (`user_uuid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_criteria`
--
ALTER TABLE `tb_criteria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_educationname`
--
ALTER TABLE `tb_educationname`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_koppel_group_rights`
--
ALTER TABLE `tb_koppel_group_rights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_level`
--
ALTER TABLE `tb_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_results`
--
ALTER TABLE `tb_results`
  MODIFY `result_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_rights`
--
ALTER TABLE `tb_rights`
  MODIFY `rights_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_schoolsubject`
--
ALTER TABLE `tb_schoolsubject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_criteria`
--
ALTER TABLE `tb_criteria`
  ADD CONSTRAINT `tb_criteria_ibfk_1` FOREIGN KEY (`form_uuid`) REFERENCES `tb_form` (`form_uuid`),
  ADD CONSTRAINT `tb_criteria_ibfk_2` FOREIGN KEY (`level_id`) REFERENCES `tb_level` (`id`);

--
-- Constraints for table `tb_results`
--
ALTER TABLE `tb_results`
  ADD CONSTRAINT `tb_results_ibfk_1` FOREIGN KEY (`criteria_id`) REFERENCES `tb_criteria` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
