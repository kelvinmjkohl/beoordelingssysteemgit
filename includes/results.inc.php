<?php  
    //require_once("../config/database.inc.php");
    
    $host = "localhost";
    $username = "root";
    $password = "";
    $database = "db_beoordelingssysteem";

    try {
        $connect = new PDO ("mysql:host=$host;dbname=$database", $username, $password);

        $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo 'Database Connection Done';

        //Fetch All Data from Table

        $query = "SELECT * FROM tb_results";

        $data = $connect->query($query);

        echo '<table width="70%" border="1" cellpadding="5" cellspacing="5"
                <tr>
                    <th>Resultaten</th>
                    <th>Student Naam</th>
                    <th>Criteria</th>
                    <th>Geslaagd</th>
                    <th>Status</th>
                </tr>';

        foreach($data as $row) {
            echo '<tr>
                    <td>' .$row["result_id"] . '</td>
                    <td>' .$row["student_uuid"] . '</td>
                    <td>' .$row["criteria_id"] . '</td>
                    <td>' .$row["checked"] . '</td>
                    <td>' .$row["status"] . '</td>
                </tr>';          
        }
        echo '</table>';
    }
    catch(PDOException $error) {
        $error->getMessage();
    }
?>